---
layout: changelog
title: Konsole 1.6.3 / KDE 3.5.3
sorted: 3053
date: 2006-05-31
css-include: /css/main.css
---

+ Fix a possible crash on session save/logout ([commit](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/konsole/konsole.cpp?rev=526028&view=log))
+ Numerous fixes due to the Coverity reports.
+ Fix DCOP call setEncoding. ([#120998](http://bugs.kde.org/120998))
+ kwrited - Add a 'Clear Messages' to the popup menu. ([#52524](http://bugs.kde.org/52524))
+ Fix crash when user deletes all the sessions. ([#121640](http://bugs.kde.org/121640))
+ Fix underline issue when there are ampersands in tab title. ([#121782](http://bugs.kde.org/121782))
+ Remember the text color while moving the tab. ([#125373](http://bugs.kde.org/125373))
+ Fix issue with tab/window titles not refreshing for the non-active session. ([#125796](http://bugs.kde.org/125796))
+ Verify that the command is executable for '<code>-e &lt;command&gt;</code>.' ([#125977](http://bugs.kde.org/125977))
+ Use the "DefaultSession" parameter in konsolerc. ([#126557](http://bugs.kde.org/126557))

