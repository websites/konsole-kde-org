---
layout: changelog
title: Konsole 1.5.2 / KDE 3.4.2
sorted: 3042
date: 2005-07-28
css-include: /css/main.css
---

+ Make 'New Window' from tab bar popup menu work.
+ Fix inital tab too big with icon only. (<a href="http://bugs.kde.org/show_bug.cgi?id=106684">#106684</a>)
+ Fix crashes when action/settings=false. (<a href="http://bugs.kde.org/show_bug.cgi?id=106829">#106829</a>)
+ Fix Konsole doesn't save Encoding setting. (<a href="http://bugs.kde.org/show_bug.cgi?id=107329">#107329 </a>)
