---
layout: changelog
title: Konsole 1.0.2 / KDE 2.2.2
sorted: 2022
css-include: /css/main.css
---

+ Added ukrainian symbol "ghe with upturn".
+ Fixed double-click marking for koi8-u encoding.
+ Fixed font selection and font save settings bugs.
+ Fixed middle after double-left click detection.
+ Remove kwrited's utmp entry at logout.
+ Removed <code>--nowelcome</code> parameter and &quot;Settings/Locale&quot; menu entry.
+ Use "Konsole Default" schema as Konsole default.
+ Added missing definitions for <kbd>Alt</kbd> + <kbd>Backspace</kbd> and "newSession" to keytables.
+ Changed order of windows title to &quot;[&lt;caption&gt;(if set) - ]&lt;session name&gt;&quot;
+ Fixed performance loss when switching desktop with sticky transparent Konsole on it.
+ Added support for esc[s and esc[u to save and restore the cursor position.
+ Setting a scroll region now takes effect on both primary and alternate screen.
+ When embedded, fixed quoting of special chars in auto "cd" command.
+ Fixed bug where window failed to close when multiple sessions were active.
