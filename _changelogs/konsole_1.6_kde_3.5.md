---
layout: changelog
title: Konsole 1.6 / KDE 3.5
sorted: 3050
date: 2005-11-29
css-include: /css/main.css
---

+ Allow user to set Tab title equal to Window title. (<a href="http://bugs.kde.org/show_bug.cgi?id=78591">#78591</a>)
+ Color in tabbar text; also possible via ESC code. (<a href="http://bugs.kde.org/show_bug.cgi?id=80725">#80725</a>)
+ Allow user to disable ZModem upload and download via kiosk. (<a href="http://bugs.kde.org/show_bug.cgi?id=81769">#81769</a>)
+ Change openterm icons to konsole icons (<a href="http://bugs.kde.org/show_bug.cgi?id=83163">#83163</a>)
+ Exit when unable to allocate a PTY. (<a href="http://bugs.kde.org/show_bug.cgi?id=87481">#87481</a>)
+ Fix some keyboard incompatibilities between konsole and real xterm. (<a href="http://bugs.kde.org/show_bug.cgi?id=92749">#92749</a>)
+ Allow konsole_part to use konsole's default settings (konsolerc) (<a href="http://bugs.kde.org/show_bug.cgi?id=94169">#94169</a>)
+ Allow xterm resize ESC code to work (<a href="http://bugs.kde.org/show_bug.cgi?id=95932">#95932</a>)
+ Fix incorrect schema in detached sessions. (<a href="http://bugs.kde.org/show_bug.cgi?id=98472">#98472</a>)
+ Use mostLocalURL on items dropped in Konsole. (<a href="http://bugs.kde.org/show_bug.cgi?id=98879">#98879</a>)
+ Add enlarge/shrink to Settings &rarr; Font menu.  Allow each session to have its own font.  Remove hard-coded fonts (unicode/console). (<a href="http://bugs.kde.org/show_bug.cgi?id=100930">#100930</a>)
+ Fix compile errors on amd64 with gcc4 (<a href="http://bugs.kde.org/show_bug.cgi?id=101559">#101559</a>)
+ Don't alter session title when using <code>--profile</code> or session management. (<a href="http://bugs.kde.org/show_bug.cgi?id=101619">#101619</a>)
+ Expand <code>~</code> in sessions' <code>Exec=</code> (<a href="http://bugs.kde.org/show_bug.cgi?id=102941">#102941</a>)
+ &quot;Monitor for Activity&quot; and &quot;Monitor for Silence&quot; icons are the same. (<a href="http://bugs.kde.org/show_bug.cgi?id=103554">#103554</a>)
+ Use correct scrollbar location for all sessions when using <code>--profile=file</code>. (<a href="http://bugs.kde.org/show_bug.cgi?id=104741">#104741</a>)
+ Fix find dialog's focus. (<a href="http://bugs.kde.org/show_bug.cgi?id=105126">#105126</a>)
+ Display a warning when entering an invalid &quot;Execute&quot; entry in the Session configuration. (<a href="http://bugs.kde.org/show_bug.cgi?id=105754">#105754</a>)
+ Fix crash when closing Konsole after detaching a session. (<a href="http://bugs.kde.org/show_bug.cgi?id=106464">#106464</a>)
+ Fix inital tab too big with icon only. (<a href="http://bugs.kde.org/show_bug.cgi?id=106684">#106684</a>)
+ Fix crashes when action/settings=false. (<a href="http://bugs.kde.org/show_bug.cgi?id=106829">#106829</a>)
+ Fix Konsole doesn't save Encoding setting. (<a href="http://bugs.kde.org/show_bug.cgi?id=107329">#107329 </a>)
+ Make sure newly activated sessions have correct schema variables. (<a href="http://bugs.kde.org/show_bug.cgi?id=111631">#111631</a>)
+ Disallow user to select Encoding &rarr; jis7 due to infinite loop. (<a href="http://bugs.kde.org/show_bug.cgi?id=114535">#114535</a>)
