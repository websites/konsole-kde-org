---
layout: changelog
title: Konsole 1.0.2 / KDE 2.2.1
sorted: 2021
css-include: /css/main.css
---

+ Many little fixes.
+ Added <code>--noxft</code> option to disable AA.
+ Added "tripple click" support.
