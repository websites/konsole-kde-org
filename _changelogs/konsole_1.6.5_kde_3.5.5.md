---
layout: changelog
title: Konsole 1.6.5 / KDE 3.5.5
sorted: 3055
date: 2006-10-11
css-include: /css/main.css
---

+ Fix wrong char at end of block for unicode. Fixes bug [131938](http://bugs.kde.org/show_bug.cgi?id=131938). See SVN commit [570191](https://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=570191&view=rev).
+ Fix Copyright/Licences missing from source files. Fixes bug [99329](https://bugs.kde.org/show_bug.cgi?id=99329)

