---
layout: changelog
title: Konsole 1.1.1 / KDE 3.0.1
sorted: 3001
css-include: /css/main.css
---

+ Fixed support for &quot;screen&quot;.
+ Drag and Drop: shellQuote single remote file.
+ Scrolllock only reacts to Key_ScrollLock, not Ctrl-S.
+ Shift-MMB now pastes for mouse-aware applications.
+ Doesn't show terminal size hint at startup.
+ Selection fixes, xterm-like behavior.
+ Session editor makes "Apply" active.
