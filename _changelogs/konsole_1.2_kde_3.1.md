---
layout: changelog
title: Konsole 1.2 / KDE 3.1
sorted: 3010
css-include: /css/main.css
---

+ Addition of "random" background colours, a "BlackOnLightColour" schema.
+ Allow to save current sessions as profile and start this with <code>--profile</code> parameter.
+ Added &quot;cutToBeginningOfLine&quot; option.
+ Made timeout for &quot;Monitor for Silence&quot; configurable.
+ kfile-bookmarks menu integration (&quot;Add Bookmark&quot; on non-Linux only with &quot;\[\e]31;\w\a\]&quot; prompt).
+ Start new shell at given bookmark (supports ssh://user@host and telnet://host like bookmarks).
+ konsolepart uses BrowserExtension::openURLRequest() (only with &quot;\[\e]31;\w\a\]&quot; prompt).
+ Session management saves initial or current (non-Linux only if set with &quot;\[\e]31;\w\a\]&quot; prompt) directory.
+ Extended drag and drop popupmenu with &quot;cp&quot;, &quot;ln&quot; and &quot;mv&quot; entries.
+ Session views are temporarily detachable from main window.
+ Improved &quot;Find in History...&quot;: Regular expressions support, &quot;Find Next&quot;, &quot;Find Previous&quot;.
+ Shortcuts are now configurable via graphical interface.
+ Added shortcuts for session switch menu, switching to first 12 sessions and font size variation.
+ Parameters for keyboard and schema selection.
+ &quot;Clear Terminal&quot; and &quot;Reset and Clear Terminal&quot; commands.
+ Optionally prevent programs from changing the window size.
+ Support for Unix98 pty devices.
+ Konsole button converted to menuext with bookmark support. (Berend De Schouwer)
+ &quot;Copy&quot; menu entries and don't write to clipboard automatically.
+ Don't prepend <kbd>ESC</kbd> when <kbd>Meta</kbd> is pressed if key definition is for "+Alt".
+ Made <kbd>Ctrl-S</kbd>/<kbd>Ctrl-Q</kbd> flow control (<kbd>Ctrl-S</kbd> freezes shell) an option with default off.
+ Added <code>--noclose</code> parameter to not close Konsole when command exits.
