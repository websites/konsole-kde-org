---
layout: changelog
title: Konsole 1.6.6 / KDE 3.5.10
sorted: 3059
date: 2008-10-16
css-include: /css/main.css
---
Bugfixes:

+ Fix zmodem download dialog not starting. Fixes bug [145177](http://bugs.kde.org/show_bug.cgi?id=145177). See SVN commit [846506](http://websvn.kde.org/?rev=846506&view=rev).

