---
layout: changelog
title: Konsole 1.2.3 / KDE 3.1.3
sorted: 3013
css-include: /css/main.css
---

+ Added Ctrl+Shift+N as alternate default shortcut for "New Session".
+ Fixed problems with mouse and paste support and detached windows.
+ Fixed fixed-width with proportional-font drawing routine.
+ Let new color schemes take effect once kcmkonsole tells to do so.
+ Wheel mouse fixes for Mode_Mouse1000.
+ "Terminal Sessions" Kicker button supports sessions with dots in filename.
+ "Terminal Sessions" Kicker button with same sort order as the one in Konsole.
