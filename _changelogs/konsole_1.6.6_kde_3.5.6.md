---
layout: changelog
title: Konsole 1.6.6 / KDE 3.5.6
sorted: 3056
date: 2007-01-25
css-include: /css/main.css
---

Bugfixes:

+ Correct calculation of colour values in 256-colour mode. See SVN commit [609838](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=609838&view=rev).
+ Fix crash when saving history twice. Fixes bug [138521](http://bugs.kde.org/show_bug.cgi?id=138521). See SVN commit [611528](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=611528&view=rev).
+ Fix crash if setting font to a size which is larger than the terminal display. See SVN commit [616760](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=616760&view=rev).


