---
layout: changelog
title: Konsole 1.3 / KDE 3.2
sorted: 3020
css-include: /css/main.css
---

+ Added a better interface to konsolepart for other applications.
+ Added "Set Selection End" to right mouse button menu.
+ Column text selecting when Ctrl and Alt are pressed.
+ "Copy"/"Paste" is now configurable with "Configure Shortcuts..."
+ Utilize KNotify (e.g. passive popup) for "Activity", "Bell", "Shell Exit" and "Silence" events
+ Support for non-fixed fonts.
+ Treat all dropped URLs into a konsole equally using kfmclient.
+ ZModem up- and download (requires rzsz).
+ <code>--noresize</code> option
+ Uses new KTabWidget allowing tab context menus, tab mouse ordering with mouse, ...
