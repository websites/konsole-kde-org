---
layout: changelog
title: KDE 4.9
sorted: 4090
css-include: /css/main.css
---

Again there are a lot of changes in KDE 4.9, some of the new features and fixes
are listed below.  

+ Seperate Konsole settings into their own dialog instead of having everything in the profile settings.
+ Add and fix a number of issues dealing with Konsole's dbus interface.
+ Add the ability to search the scrollback in Konsole's KPart.
+ Add support for dragging tab out of Konsole's window.
+ Add support for KDE Web shortcuts in the context menu.
+ Add text alerting user that when using unlimited scrollback it will be saved to HD.
+ Add a 'clone' tab menu option.

+ Allow dragging shell out of konsole window. (<a href="http://bugs.kde.org/show_bug.cgi?id=56749">56749</a>)[<a href="http://commits.kde.org/konsole/b8b8ef7405e81dfcbe63e5a1b84cf773b6a36096">b8b8ef7</a>]
+ Add dbus call to set the number of lines in history.  (<a href="http://bugs.kde.org/show_bug.cgi?id=120040">120040</a>)[<a href="http://commits.kde.org/konsole/95b8d5551e9cf7dae76854b9f31d1374c5e3aee5">95b8d55</a>]
+ Add support for direct 24-bit color specifications in escape sequences.  (<a href="http://bugs.kde.org/show_bug.cgi?id=138740">138740</a>)[<a href="http://commits.kde.org/konsole/35e9cd847a236e4b495cb1c03347ee8f59a4d8e8">35e9cd8</a>]
+ Add embedded terminal notifying the host about changes in current working directory.  (<a href="http://bugs.kde.org/show_bug.cgi?id=156919">156919</a>)[<a href="http://commits.kde.org/konsole/675dbcb2fe777b22044510579321148989d29cfb">675dbcb</a>]
+ Add ability to search scrollback in KPart.  (<a href="http://bugs.kde.org/show_bug.cgi?id=162319">162319</a>)[<a href="http://commits.kde.org/konsole/cce19d8cb859c1d0e3bc2f019f68aa9b864a87f2">cce19d8</a>]
+ Add ability to define separate window and tab title formats.  (<a href="http://bugs.kde.org/show_bug.cgi?id=162326">162326</a>)[<a href="http://commits.kde.org/konsole/4fb4da4c752b4781344f6eb9306780c35613d1bb">4fb4da4</a>]
+ When Konsole is mimimized, always show 'close tabs' dialog when quitting.  (<a href="http://bugs.kde.org/show_bug.cgi?id=163677">163677</a>)[<a href="http://commits.kde.org/konsole/66c0ddf7d1d5c9a7009f33da418d27a186651b92">66c0ddf</a>]
+ Konsole should use new process when some Qt/KDE options are given. (<a href="http://bugs.kde.org/show_bug.cgi?id=179010">179010</a>)[<a href="http://commits.kde.org/konsole/6b811ec2b6e4fbf459af9765c9ead506c9a0cd3c">6b811ec</a>]
+ Allow the setting of the tab title and window title independently.  (<a href="http://bugs.kde.org/show_bug.cgi?id=179142">179142</a>)[<a href="http://commits.kde.org/konsole/363a2701774ffd0b14bfd8223a5848179a427bd6">363a270</a>]
+ Konsole auto copy selection to clipboard to avoid confusion between shortcuts for pasting from clipboard and selection.  (<a href="http://bugs.kde.org/show_bug.cgi?id=183490">183490</a>)[<a href="http://commits.kde.org/konsole/bb818010b42a3d6fcdc4a057f0afd2e7b7a85b9f">bb81801</a>]
+ Add option to cd into dropped directories.  (<a href="http://bugs.kde.org/show_bug.cgi?id=197867">197867</a>)[<a href="http://commits.kde.org/konsole/c08484f40a424825490cb297e33046cb5ffdad5a">c08484f</a>]
+ Add more KONSOLE_DBUS_ env variables.  (<a href="http://bugs.kde.org/show_bug.cgi?id=227296">227296</a>)[<a href="http://commits.kde.org/konsole/debfec2eb3c8ede89977b1f7d755b9f0d8a31e31">debfec2</a>]
+ Make it possible to obtain the default profile using dbus (<a href="http://bugs.kde.org/show_bug.cgi?id=252532">252532</a>)[<a href="http://commits.kde.org/konsole/52f656663f539df8a53a8a2627e569d0c4065a7a">52f6566</a>]
+ Add a way to open new identical session in a tab with menu (Shortcut is working fine) (<a href="http://bugs.kde.org/show_bug.cgi?id=254976">254976</a>)[<a href="http://commits.kde.org/konsole/56acddac933b96c10c782a918a419f6aaf4b2702">56acdda</a>]
+ Konsole shortcut "Ctrl + Shift + X" for clearing scroll and reset appears as problematic (<a href="http://bugs.kde.org/show_bug.cgi?id=282593">282593</a>)[<a href="http://commits.kde.org/konsole/dad64e67e73df7a6968cfdfdd8741531ec381dc9">dad64e6</a>]
+ RFE - Add ability to send commands to Konsole via D-BUS (<a href="http://bugs.kde.org/show_bug.cgi?id=283719">283719</a>)[<a href="http://commits.kde.org/konsole/ba204b106577aa0e1c5662272d68b17a9e35cc9b">ba204b1</a>]
+ Support extended mouse coordinates (<a href="http://bugs.kde.org/show_bug.cgi?id=285984">285984</a>)[<a href="http://commits.kde.org/konsole/b876f2a3edcb63f29588fdde6b20dc6c9d13bc24">b876f2a</a>]
+ Removing Konsole menubar but is brought back after reboot (<a href="http://bugs.kde.org/show_bug.cgi?id=288768">288768</a>]
+ New tab shows local terminal instead of remote when running konsole via ssh kio (<a href="http://bugs.kde.org/show_bug.cgi?id=292518">292518</a>)[<a href="http://commits.kde.org/konsole/56acddac933b96c10c782a918a419f6aaf4b2702">56acdda</a>]
+ Open close query when logging out with >1 sessions running. (<a href="http://bugs.kde.org/show_bug.cgi?id=127194">127194</a>)[<a href="http://commits.kde.org/konsole/3309e5b12280b27a6e632f9c5e43a38f8f0791bf">3309e5b</a>]
+ Make it possible to obtain the default profile using dbus (<a href="http://bugs.kde.org/show_bug.cgi?id=252532">252532</a>)[<a href="http://commits.kde.org/konsole/52f656663f539df8a53a8a2627e569d0c4065a7a">52f6566</a>]

And then, there's this:

+ Modification of default profile is creating new profile. (<a href="http://bugs.kde.org/show_bug.cgi?id=250506">250506</a>)[<a href="http://commits.kde.org/konsole/6fa0121b8ef31934153767678ed18467e900eca0">6fa0121</a>]
+ Alt key does not open menu, Alt+&lt;key&gt; shortcuts do not work when terminal has focus (<a href="http://bugs.kde.org/show_bug.cgi?id=154755">154755</a>]
+ Not all settings in new profile are applied after the  'change profile' action is triggered (<a href="http://bugs.kde.org/show_bug.cgi?id=171866">171866</a>]
+ Konsole does not set initial working directory from cwd (<a href="http://bugs.kde.org/show_bug.cgi?id=187754">187754</a>)[<a href="http://commits.kde.org/konsole/ebcab3c307f83ddf42a9096b8a76a5389ce52f3d">ebcab3c</a>]
+ Disable the advanced feature of "CommonDirNames" for "%d" by default (<a href="http://bugs.kde.org/show_bug.cgi?id=190281">190281</a>)[<a href="http://commits.kde.org/konsole/5a936adb2005c4da88b457f4b6625dc427a878e3">5a936ad</a>]
+ Don't expose Konsole actions to hosting application (<a href="http://bugs.kde.org/show_bug.cgi?id=248469">248469</a>)[<a href="http://commits.kde.org/konsole/08de49da1cf4c89c375d7eea267bce3b46c05527">08de49d</a>]
+ Seperate Konsole settings from profile settings. (<a href="http://bugs.kde.org/show_bug.cgi?id=250508">250508</a>]
+ Selecting text by triple click and scolling up causes only the visible contents to be selected (<a href="http://bugs.kde.org/show_bug.cgi?id=256353">256353</a>)[<a href="http://commits.kde.org/konsole/6d9d49aafb358293326f4edca393c7f2dfc9602a">6d9d49a</a>]
+ Add more 'What's this?" text (<a href="http://bugs.kde.org/show_bug.cgi?id=257914">257914</a>)[<a href="http://commits.kde.org/konsole/eec927bd887f1939dbc544df7d795a9cf282634d">eec927b</a>]
+ Tab name doesn't revert immediately after running some commands (<a href="http://bugs.kde.org/show_bug.cgi?id=271275">271275</a>)[<a href="http://commits.kde.org/konsole/2cf9715edd9fb1e38afb42daca4aa7686ccccd43">2cf9715</a>]
+ newSession() dbus call does not allows to open new session in specified window (<a href="http://bugs.kde.org/show_bug.cgi?id=276912">276912</a>)[<a href="http://commits.kde.org/konsole/07cddfe302233c3555258f077429e55ce622e262">07cddfe</a>]
+ Konsole lose paste  when clicking middle mouse button quickly and successively (<a href="http://bugs.kde.org/show_bug.cgi?id=280332">280332</a>)[<a href="http://commits.kde.org/konsole/a123872f295c3c15f45157162d2f06b272bc99ae">a123872</a>]
+ dbus method org.kde.konsole.Konsole.currentSession always return the active session within the first window, even when the qdbus commnd is executed in some session within the second window. (<a href="http://bugs.kde.org/show_bug.cgi?id=281513">281513</a>)[<a href="http://commits.kde.org/konsole/07cddfe302233c3555258f077429e55ce622e262">07cddfe</a>]
+ konsolerc: usage of full path for some config values is not portable between accounts (<a href="http://bugs.kde.org/show_bug.cgi?id=283102">283102</a>)[<a href="http://commits.kde.org/konsole/2663d9c712f5fa9e707b98e4ea41fe50c99585e8">2663d9c</a>]
+ [PATCH] "konsole --nofork" crashes when started not from terminal (<a href="http://bugs.kde.org/show_bug.cgi?id=288200">288200</a>)[<a href="http://commits.kde.org/konsole/595ccda304f4a1c6a54039ce2f5d2be0f1ba3c41">595ccda</a>]
+ Konsole dbus sessions count is broken (<a href="http://bugs.kde.org/show_bug.cgi?id=292309">292309</a>)[<a href="http://commits.kde.org/konsole/07cddfe302233c3555258f077429e55ce622e262">07cddfe</a>]
+ Profile setting "Show in Menu" not saved (<a href="http://bugs.kde.org/show_bug.cgi?id=292637">292637</a>)[<a href="http://commits.kde.org/konsole/0bd1927585fe043e2a9a97adbe188f59747e5f83">0bd1927</a>]
+ Konsole should migrate user's tab settins from the profile to the global settings (<a href="http://bugs.kde.org/show_bug.cgi?id=293231">293231</a>)[<a href="http://commits.kde.org/konsole/f542899f13a7a7e9aab297a4e45877b862ceacba">f542899</a>]
+ Command " konsole -e 'man ls' " does not work, while "konsole -e man ls" works fine (<a href="http://bugs.kde.org/show_bug.cgi?id=295648">295648</a>)[<a href="http://commits.kde.org/konsole/aa75fc8fee7e55e0c78640fcbed0bf0627ba7971">aa75fc8</a>]
+ konsole does not always honor '--display' (<a href="http://bugs.kde.org/show_bug.cgi?id=297801">297801</a>)[<a href="http://commits.kde.org/konsole/6b811ec2b6e4fbf459af9765c9ead506c9a0cd3c">6b811ec</a>]
+ Alt key does not open menu, Alt+&lt;key&gt; shortcuts do not work when terminal has focus (<a href="http://bugs.kde.org/show_bug.cgi?id=154755">154755</a>]
+ Not all settings in new profile are applied after the  'change profile' action is triggered (<a href="http://bugs.kde.org/show_bug.cgi?id=171866">171866</a>]
+ Selecting text by triple click and scolling up causes only the visible contents to be selected (<a href="http://bugs.kde.org/show_bug.cgi?id=256353">256353</a>)[<a href="http://commits.kde.org/konsole/6d9d49aafb358293326f4edca393c7f2dfc9602a">6d9d49a</a>]
+ Tab name doesn't revert immediately after running some commands (<a href="http://bugs.kde.org/show_bug.cgi?id=271275">271275</a>)[<a href="http://commits.kde.org/konsole/2cf9715edd9fb1e38afb42daca4aa7686ccccd43">2cf9715</a>]
+ dbus method org.kde.konsole.Konsole.currentSession always return the active session within the first window, even when the qdbus commnd is executed in some session within the second window. (<a href="http://bugs.kde.org/show_bug.cgi?id=281513">281513</a>)[<a href="http://commits.kde.org/konsole/07cddfe302233c3555258f077429e55ce622e262">07cddfe</a>]
+ konsolerc: usage of full path for some config values is not portable between accounts (<a href="http://bugs.kde.org/show_bug.cgi?id=283102">283102</a>)[<a href="http://commits.kde.org/konsole/2663d9c712f5fa9e707b98e4ea41fe50c99585e8">2663d9c</a>]
+ [PATCH] "konsole --nofork" crashes when started not from terminal (<a href="http://bugs.kde.org/show_bug.cgi?id=288200">288200</a>)[<a href="http://commits.kde.org/konsole/595ccda304f4a1c6a54039ce2f5d2be0f1ba3c41">595ccda</a>]
+ Konsole dbus sessions count is broken (<a href="http://bugs.kde.org/show_bug.cgi?id=292309">292309</a>)[<a href="http://commits.kde.org/konsole/07cddfe302233c3555258f077429e55ce622e262">07cddfe</a>]
+ Profile setting "Show in Menu" not saved (<a href="http://bugs.kde.org/show_bug.cgi?id=292637">292637</a>)[<a href="http://commits.kde.org/konsole/0bd1927585fe043e2a9a97adbe188f59747e5f83">0bd1927</a>]
+ Konsole should migrate user's tab settins from the profile to the global settings (<a href="http://bugs.kde.org/show_bug.cgi?id=293231">293231</a>)[<a href="http://commits.kde.org/konsole/f542899f13a7a7e9aab297a4e45877b862ceacba">f542899</a>]
+ flow control: possible to continue stopped flow without Konsole noticing (<a href="http://bugs.kde.org/show_bug.cgi?id=293534">293534</a>)[<a href="http://commits.kde.org/konsole/b9a69ec009d69d14561c07efc8559e4d6a708777">b9a69ec</a>]
