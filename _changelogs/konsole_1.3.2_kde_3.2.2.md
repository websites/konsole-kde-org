---
layout: changelog
title: Konsole 1.3.2 / KDE 3.2.2
sorted: 3022
css-include: /css/main.css
---

+ Added support for Scroll Up (SU) and Scroll Down (SD)
+ Better compatibility with xterm/XF86 4.4.0
+ Fixed sending of large blocks
+ Show session menu when you move the mouse. (<a href="http://bugs.kde.org/show_bug.cgi?id=77873">#77873</a>)
