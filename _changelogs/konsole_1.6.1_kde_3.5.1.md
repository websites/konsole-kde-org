---
layout: changelog
title: Konsole 1.6.1 / KDE 3.5.1
sorted: 3051
date: 2006-01-31
css-include: /css/main.css
---

+ Update blue/red icons to be more distinguishable. ([#117065](http://bugs.kde.org/show_bug.cgi?id=117065))
+ The history options (line #/enabled) are now used in the profiles. ([#120046](http://bugs.kde.org/show_bug.cgi?id=120046))

