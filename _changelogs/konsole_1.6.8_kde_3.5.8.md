---
layout: changelog
title: Konsole 1.6.8 / KDE 3.5.8
sorted: 3058
date: 2007-10-16
css-include: /css/main.css
---

Bugfixes:

+ Real transparency support without side effects, if qt-copy patch #0078 is available. See SVN commit [669488](http://websvn.kde.org/?rev=669488&view=rev).


