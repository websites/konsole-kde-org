---
layout: changelog
title: KDE Applications 21.04.0
sorted: 21040
date: 2021-12-13
css-include: /css/main.css
---

- Fix crash when using a color scheme with random colors
- Use inverted colours when calculated fancy BG has too low contrast
- Improve appearance of text selection by blending foreground & background
- Pressing ESC when the search bar is unfocused should not close search bar
- Reflow lines when terminal resizes
- EditProfileDialog: add combobox to select custom text editor
- EditProfileDialog: add GUI to change the terminal bell mode
- Update to Unicode 13.0.0
- SessionContoller: fix crash when closing session from Konsole KPart
- Add menu items Enlarge / Shrink Fonts
- Convert files to use SPDX license/copyright
