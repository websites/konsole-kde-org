---
layout: changelog
title: Konsole 1.6.2 / KDE 3.5.2
sorted: 3052
date: 2006-03-28
css-include: /css/main.css
---

+ Correct issue where history size is unlimited when dealing with History options in profiles (bug [#120046](http://bugs.kde.org/show_bug.cgi?id=120046))
+ Correctly set Tab bar when set to Dynamic Hide after session restore (bug [#121688](http://bugs.kde.org/show_bug.cgi?id=121688))

