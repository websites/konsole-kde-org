---
layout: changelog
title: Konsole 1.5 / KDE 3.4
sorted: 3040
css-include: /css/main.css
---

+ There are two major internal changes:
    + The hiding of the tabbar is now done via ktabwidget &rarr; setTabBarHidden(). This greatly simplifies hiding and unhiding of the tabbar. This also closes a number of bugs related to the tabbar. **This has the side effect of adding extra pixels around the terminal border**.
    + The detaching of sessions now uses a Konsole window instead of a konsole_child.  This removes duplicate code and simplifies the code. **Also, re-attaching a detached session is no longer possible**.
+ **The Help file and Tip file have been completely updated**.
+ Option to preserve the scrollback buffer after a screen clear (<a href="http://bugs.kde.org/show_bug.cgi?id=55343">#55343</a>)
+ Line drawing characters do not join vertically (<a href="http://bugs.kde.org/show_bug.cgi?id=61637">#61637</a>)
+ Add shortcuts for all sessions (<a href="http://bugs.kde.org/show_bug.cgi?id=66737">#66737</a>)
+ Resize tab size when the number of tabs does not with on the toolbar (<a href="http://bugs.kde.org/show_bug.cgi?id=77528">#77528</a>)
+ Starting with <code>--profile</code> and <code>--notabbar</code> crashes (<a href="http://bugs.kde.org/show_bug.cgi?id=81522">#81522</a>)
+ Option <code>--notabbar</code> interferes with <code>--schema=&lt;transparent schema&gt;</code> (<a href="http://bugs.kde.org/show_bug.cgi?id=83162">#83162</a>)
+ Add support for IUTF8 (<a href="http://bugs.kde.org/show_bug.cgi?id=83236">#83236</a>)
+ Copy / paste removes whitespace if it is the last character of wrapping line (<a href="http://bugs.kde.org/show_bug.cgi?id=90201">#90201</a>)
+ Incorrect terminal size before first visited when using <code>--profile=</code> (<a href="http://bugs.kde.org/show_bug.cgi?id=90309">#90309</a>)
+ Workdir option sets starting directory only for the very first session in konsole (<a href="http://bugs.kde.org/show_bug.cgi?id=94864">#94864</a>)
+ Changing history lines count to 1 causes a crash while scrolling up (<a href="http://bugs.kde.org/show_bug.cgi?id=95990">#95900</a>)
+ Scrolling slow after showing some japanese characters (<a href="http://bugs.kde.org/show_bug.cgi?id=98410">#98410</a>)
+ Don't crash when selecting full line with <kbd>Ctrl+Alt</kbd> (<a href="http://bugs.kde.org/show_bug.cgi?id=100308">#100308</a>)
