---
layout: changelog
title: Konsole 1.3.1 / KDE 3.2.1
sorted: 3021
css-include: /css/main.css
---

+ Made sending of master input to added session working (<a href="http://bugs.kde.org/show_bug.cgi?id=73695">#73695</a>)
+ Fixed dynamic toolbar hiding initialization (<a href="http://bugs.kde.org/show_bug.cgi?id=75638">#75638</a>)
+ Handle schemas with absolute paths (<a href="http://bugs.kde.org/show_bug.cgi?id=73997">#73997</a>)
+ Fixed crash with <code>--noscrollbar</code> (<a href="http://bugs.kde.org/show_bug.cgi?id=74152">#74152</a>)
+ Settings/keyboard entries are now sorted (<a href="http://bugs.kde.org/show_bug.cgi?id=74269">#74269</a>)
+ <kbd>Ctrl-C</kbd> killed konsole window, not processes running in shell (<a href="http://bugs.kde.org/show_bug.cgi?id=73226">#73226</a>)
