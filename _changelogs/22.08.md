---
layout: changelog
title: KDE Gear 22.08.0
sorted: 22080
date: 2022-07-13
css-include: /css/main.css
---

- Add action to setup semantic shell integration (shortcut: ctrl-alt-])
- Add profile options to enable semantic integration options
- Add a "Show session" button to notifications
- Allow up and down arrow to move up/down in multiline input
- Show semantic shell visual hints When showing URL hints
- Add semantic shell integration - Add `OSC 133 ; *`
- VT parser: Support chars >= 0xA0 in OSC parser
- New parser based on the vt100.net diagram
- Improve usability on macOS including altering shortcuts
- Fix z-modem detection by using proper string index
- Only recognize URIs with balanced parentheses
- URI regexp: support ? in query and fragment
- URI regexp: allow more than one colon in userinfo
- Updated DEC Line Graphics character set to use Unicode 3.2.0
- Remove zlib in favor of Qt built-in qUncompress for graphics
- Add support for `OSC 4` and `OSC 104` for managing the text color table
