---
layout: changelog
title: Konsole 1.1.2 / KDE 3.0.2
sorted: 3002
css-include: /css/main.css
---

+ Strengthened security against text injection.
