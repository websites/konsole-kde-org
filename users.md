---
layout: page
title: User support
konqi: /assets/img/konqi-docbook.png
sorted: 4
---

# User support

## Handbook

Lost in Konsole settings or just looking to discover more features?
You can read the [Konsole Handbook](https://docs.kde.org/stable5/en/konsole/konsole/index.html)
to find answers to your questions.

## Need Help?

Check Konsole on [KDE Forums](http://forum.kde.org/viewforum.php?f=227).
Perhaps someone has already solved a similar problem. And if not, feel free to
start a new topic!

## Found a Bug?

Bugs in software happen. If you found some in Konsole, please report them to
[our Bugzilla](https://bugs.kde.org/enter_bug.cgi?product=konsole). Please do
not report bugs on forums or mailing lists - such reports will most likely be
overlooked or forgotten by the developers quite soon after. To make sure the
bug gets noticed by developers and fixed as soon as possible, report it into
Bugzilla.

## Missing a Feature?

Konsole has a lot of features, but there's always a room for more. If you have
an idea for an improvement or you are missing a feature in Konsole, write your
suggestion in our [Bugzilla](https://bugs.kde.org), so that the developers can
find it and implement it.

