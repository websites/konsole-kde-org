---
layout: page
title: Get Involved
konqi: /assets/img/konqi-dev.png
sorted: 5
---

# Get Involved!

Want to make Konsole better? Consider getting involved in Konsole development
and help us make Konsole the best terminal emulator!

## Build Konsole from Source

The [community wiki](https://community.kde.org/Get_Involved/development)
provides excellent resources for setting up your very own development
environment.

## Get in Touch!

Most development-related discussions take place on the [konsole-devel mailing
list](http://mail.kde.org/mailman/listinfo/konsole-devel). Just join in, say hi
and tell us what you would like to help us with!

## Not a Programmer?

Not a problem! There's plenty of other tasks that you can help us with to
make Konsole better, even if you don't know any programming languages!

* [Bug triaging](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging) - help us find
  mis-filed, duplicated or invalid bug reports in Bugzilla
* [Localization](https://community.kde.org/Get_Involved/translation) - help to translate
  Konsole into your language
* [Documentation](https://community.kde.org/Get_Involved/documentation) - help us improve user
  documentation to make Konsole more friendly for newcomers
* [Promotion](https://community.kde.org/Get_Involved/promotion) - help us promote Konsole
  both online and offline
* [Updating wiki](https://userbase.kde.org/Konsole) - help update the information present in
  the wiki, add new tutorials, etc. - help make it easier for others to join!
* Do you have any other idea? Get in touch!

