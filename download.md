---
layout: page
title: Download
css-include: /css/download.css
sorted: 3

sources:
  - name: Linux
    icon: /assets/img/tux.png
    description: >
        Konsole is already available on the majority of Linux distributions.
        You can install it from the <a href="appstream://org.kde.konsole.desktop">Discover</a> or
        <a href="appstream://org.kde.konsole.desktop">GNOME Software</a>.
  - name: Release Sources
    icon: /assets/img/ark.svg
    description: >
        Konsole is released regularly as part of KDE release service. You can
        find Konsole latest stable release among the
        <a href="https://download.kde.org/stable/release-service/">tarballs from
        the latest KDE applications release</a>.

        If you want to build Konsole from source, we recommend checking out our
        <a href="get-involved.html">Getting Involved</a> page, which contains links to
        a full guide how to compile Konsole yourself.
  - name: Git
    icon: /assets/img/git.svg
    description: >
        Konsole git repository can be viewed
        <a href="https://invent.kde.org/utilities/konsole">using KDE's GitLab instance</a>.

        To clone Konsole, use <code>git clone
        https://invent.kde.org/utilities/konsole.git</code>. For detailed instructions on how
        to build Konsole from source, check the <a href="get-involved.html">Getting
        Involved page</a>
---

<h1>Download</h1>

<table class="distribution-table">
{% for source in page.sources %}
    <tr class="title-row">
        <td rowspan="2" width="100">
            <img src="{{ source.icon }}" alt="{{ source.name }}">
        </td>
        <th>{{ source.name }}</th>
    </tr>
    <tr>
        <td>{{ source.description }}</td>
    </tr>
{% endfor %}
</table>
